let persons = [
  {
    first_name: 'Saikat',
    last_name: 'Halder'
  },
  {
    first_name: 'Saikat',
    last_name: 'Halder'
  },
  {
    first_name: 'Soma',
    last_name: 'Halder'
  },
  {
    first_name: 'Tapas',
    last_name: 'Halder'
  },
  {
    first_name: 'Dipsikha',
    last_name: 'Halder'
  },
  {
    first_name: 'Dipsikha',
    last_name: 'Halder'
  }
];
persons.forEach(person => {
  let count;
  count = persons.filter(p => p.first_name == person.first_name);
  if (count.length > 1) {
    console.log(`%cnot unique -> ${count[0].first_name}`, 'color: red');
  } else {
    console.log('%cunique names are there', 'color: green');
  }
});
