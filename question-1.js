function findAngle(hour, min) {
  let h = (hour * 360) / 12 + (min * 360) / (12 * 60);
  let m = (min * 360) / 60;
  let angle = Math.abs(h - m);
  if (angle > 180) {
    angle = 360 - angle;
  }
  console.log(angle);
}

findAngle(10, 30);
